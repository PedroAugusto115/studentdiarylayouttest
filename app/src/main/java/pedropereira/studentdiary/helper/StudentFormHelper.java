package pedropereira.studentdiary.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import pedropereira.studentdiary.R;
import pedropereira.studentdiary.activities.StudentFormActivity;
import pedropereira.studentdiary.model.Student;

/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class StudentFormHelper {

    private final RatingBar ratingField;
    private final EditText websiteField;
    private final EditText phoneField;
    private final EditText addressField;
    private final EditText nameField;
    private final ImageView photoField;

    private Student student;

    public StudentFormHelper(StudentFormActivity activity){
        nameField      = (EditText)activity.findViewById(R.id.student_form_name_txt);
        addressField   = (EditText)activity.findViewById(R.id.student_form_address_txt);
        phoneField     = (EditText)activity.findViewById(R.id.student_form_phone_txt);
        websiteField   = (EditText)activity.findViewById(R.id.student_form_site_txt);
        ratingField    = (RatingBar)activity.findViewById(R.id.student_form_rtn_bar);
        photoField     = (ImageView)activity.findViewById(R.id.student_form_photo);
        student = new Student();
    }

    public Student getStudent(){
        student.setName(nameField.getText().toString());
        student.setAddress(addressField.getText().toString());
        student.setPhone(phoneField.getText().toString());
        student.setWebsite(websiteField.getText().toString());
        student.setRating(Double.valueOf(ratingField.getProgress()));
        student.setPhotoPath((String) photoField.getTag());
        return student;
    }

    public void fillStudentForm(Student student) {
        nameField.setText(student.getName());
        addressField.setText(student.getAddress());
        phoneField.setText(student.getPhone());
        websiteField.setText(student.getWebsite());
        ratingField.setProgress(student.getRating().intValue());
        loadImage(student.getPhotoPath());
        this.student = student;
    }

    public void loadImage(String path) {
        if(path != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            Bitmap smallerBitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            photoField.setImageBitmap(smallerBitmap);
            photoField.setScaleType(ImageView.ScaleType.FIT_XY);
            photoField.setTag(path);
        }
    }
}