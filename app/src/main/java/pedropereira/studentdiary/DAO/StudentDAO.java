package pedropereira.studentdiary.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import pedropereira.studentdiary.model.Student;

/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class StudentDAO extends SQLiteOpenHelper {


    private List<Student> students;

    public StudentDAO(Context context) {
        super(context, "StudentDiary", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Student (id INTEGER PRIMARY KEY, " +
                "name TEXT NOT NULL, " +
                "address TEXT NOT NULL, " +
                "phone TEXT, " +
                "website TEXT, " +
                "rating REAL," +
                "photo TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "";
        switch (oldVersion){
            case 1:
                sql = "ALTER TABLE Student ADD COLUMN photo TEXT";
                db.execSQL(sql);
        }

    }

    public void insertStudent(Student student){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues data = getContentValues(student);

        db.insert("Student", null, data);
    }

    public List<Student> getStudents() {
        List<Student> students = new ArrayList<Student>();
        String sql = "SELECT * FROM Student";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        while(c.moveToNext()){
            Student student = new Student();
            student.setId(c.getLong(c.getColumnIndex("id")));
            student.setName(c.getString(c.getColumnIndex("name")));
            student.setAddress(c.getString(c.getColumnIndex("address")));
            student.setPhone(c.getString(c.getColumnIndex("phone")));
            student.setWebsite(c.getString(c.getColumnIndex("website")));
            student.setRating(c.getDouble(c.getColumnIndex("rating")));
            student.setPhotoPath(c.getString(c.getColumnIndex("photo")));

            students.add(student);
        }
        c.close();
        return students;
    }

    public void delete(Student student) {
        SQLiteDatabase db = getWritableDatabase();

        String[] params = {student.getId().toString()};

        db.delete("Student", "id = ?", params);
    }

    public void updateStudent(Student student) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues data = getContentValues(student);

        String[] params = {student.getId().toString()};

        db.update("Student", data, "id = ?", params);
    }

    public boolean isStudentByPhone(String phone){
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM Student WHERE phone = ?", new String[]{phone});

        int result = c.getCount();

        c.close();

        return result > 0;
    }

    @NonNull
    private ContentValues getContentValues(Student student) {
        ContentValues data = new ContentValues();

        data.put("name", student.getName());
        data.put("address", student.getAddress());
        data.put("phone", student.getPhone());
        data.put("website", student.getWebsite());
        data.put("rating", student.getRating());
        data.put("photo", student.getPhotoPath());
        return data;
    }
}

