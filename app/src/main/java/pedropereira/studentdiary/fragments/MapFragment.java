package pedropereira.studentdiary.fragments;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import pedropereira.studentdiary.DAO.StudentDAO;
import pedropereira.studentdiary.GPS.Locator;
import pedropereira.studentdiary.model.Student;

/**
 * Created by Pedro Augusto on 22/08/2016.
 */
public class MapFragment extends SupportMapFragment implements OnMapReadyCallback {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng initialPosition = getCoordinatsFromAddress("Rua Vergueiro 3185, Vila Mariana, Sao Paulo");

        if(initialPosition != null) {
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(initialPosition, 13);
            googleMap.moveCamera(update);
        }

        new Locator(getContext(), googleMap);

        markStudentsOnMap(googleMap);
    }

    private LatLng getCoordinatsFromAddress(String address) {
        Geocoder geocoder = new Geocoder(getContext());

        try {
            List<Address> geocoderResult = geocoder.getFromLocationName(address, 1);

            if(!geocoderResult.isEmpty()){
                LatLng position = new LatLng(geocoderResult.get(0).getLatitude(),
                        geocoderResult.get(0).getLongitude());
                return position;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void markStudentsOnMap(GoogleMap googleMap){

        StudentDAO studentDao = new StudentDAO(getContext());

        for (Student student : studentDao.getStudents()){
            LatLng studentAddress = getCoordinatsFromAddress(student.getAddress());
            if(studentAddress != null){
                MarkerOptions marker = new MarkerOptions();
                marker.position(studentAddress);
                marker.title(student.getName());
                marker.snippet(String.valueOf(student.getRating()));
                googleMap.addMarker(marker);
            }
        }

        studentDao.close();

    }
}
