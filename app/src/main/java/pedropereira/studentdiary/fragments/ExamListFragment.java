package pedropereira.studentdiary.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import pedropereira.studentdiary.R;
import pedropereira.studentdiary.activities.ExamDetailsActivity;
import pedropereira.studentdiary.activities.ExamsListActivity;
import pedropereira.studentdiary.model.Exam;

/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class ExamListFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_exams_list, container, false);

        List<String> portugueseTopcs = Arrays.asList("Sujeito", "Objeto direto", "Objeto Indireto");
        Exam portugueseExam = new Exam("Portuguese", "16/08/2016", portugueseTopcs);

        List<String> mathTopcs = Arrays.asList("Equações de segundo grau", "Trigonometria");
        Exam mathExam = new Exam("Math", "25/08/2016", mathTopcs);

        List<Exam> exams = Arrays.asList(portugueseExam, mathExam);

        ArrayAdapter<Exam> adapter = new ArrayAdapter<Exam>(getContext(), android.R.layout.simple_list_item_1, exams);

        ListView list = (ListView) view.findViewById(R.id.exam_list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Exam exam = (Exam) parent.getItemAtPosition(position);
                Toast.makeText(getContext(), "Pressed " + exam.toString() + " exam", Toast.LENGTH_SHORT).show();

                ExamsListActivity examsActivity = (ExamsListActivity) getActivity();
                examsActivity.selectExam(exam);
            }
        });

        return view;
    }
}
