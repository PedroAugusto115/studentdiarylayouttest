package pedropereira.studentdiary.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import pedropereira.studentdiary.R;
import pedropereira.studentdiary.model.Exam;

public class ExamDetailsFragment extends Fragment {

    private TextView subjectField;
    private TextView dateField;
    private ListView listTopicsField;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_exam_details, container, false);

        subjectField = (TextView) view.findViewById(R.id.exam_details_subject);
        dateField = (TextView) view.findViewById(R.id.exam_details_date);
        listTopicsField = (ListView) view.findViewById(R.id.exam_details_topics);

        Bundle parameters = getArguments();

        if(parameters != null){
            Exam exam = (Exam) parameters.getSerializable("Exam");
            fillFieldsWithExam(exam);
        }

        return view;
    }

    public void fillFieldsWithExam(Exam exam){
        subjectField.setText(exam.getSubject());
        dateField.setText(exam.getDate());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, exam.getTopics());

        listTopicsField.setAdapter(adapter);
    }

}
