package pedropereira.studentdiary.converter;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

import pedropereira.studentdiary.model.Student;

/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class StudentConverter {

    public String convertListToJson(List<Student> students) {
        JSONStringer js = new JSONStringer();

        try {

            js.object().key("list").array().object().key("student").array();

            for(Student student : students){
                js.object();
                js.key("id").value(student.getId());
                js.key("nome").value(student.getName());
                js.key("endereco").value(student.getAddress());
                js.key("telefone").value(student.getPhone());
                js.key("site").value(student.getWebsite());
                js.key("nota").value(student.getRating());
                js.endObject();
            }

            js.endArray().endObject().endArray().endObject();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js.toString();
    }
}