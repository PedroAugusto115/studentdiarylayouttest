package pedropereira.studentdiary.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import pedropereira.studentdiary.R;
import pedropereira.studentdiary.fragments.ExamDetailsFragment;
import pedropereira.studentdiary.fragments.ExamListFragment;
import pedropereira.studentdiary.model.Exam;

public class ExamsListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exams_list);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction tx = fragmentManager.beginTransaction();

        tx.replace(R.id.principal_frame, new ExamListFragment());

        if(isInLandscapeMode()){
            tx.replace(R.id.secondary_frame, new ExamDetailsFragment());
        }

        tx.commit();
    }

    private boolean isInLandscapeMode() {
        return getResources().getBoolean(R.bool.LandscapeModel);
    }

    public void selectExam(Exam exam) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        if(!isInLandscapeMode()) {
            ExamDetailsFragment examDetailsFrag = new ExamDetailsFragment();
            Bundle parameters = new Bundle();
            parameters.putSerializable("Exam", exam);
            examDetailsFrag.setArguments(parameters);

            FragmentTransaction tx = fragmentManager.beginTransaction();
            tx.replace(R.id.principal_frame, examDetailsFrag);
            tx.addToBackStack(null);//necessary to back to exam list and not to student list
            tx.commit();

        } else {

            ExamDetailsFragment examDetailsFrag =
                    (ExamDetailsFragment) fragmentManager.findFragmentById(R.id.secondary_frame);
            examDetailsFrag.fillFieldsWithExam(exam);

        }

    }
}
