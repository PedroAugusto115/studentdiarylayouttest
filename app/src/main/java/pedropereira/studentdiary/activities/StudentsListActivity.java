package pedropereira.studentdiary.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import pedropereira.studentdiary.DAO.StudentDAO;
import pedropereira.studentdiary.R;
import pedropereira.studentdiary.adapter.StudentAdapter;
import pedropereira.studentdiary.model.Student;
import pedropereira.studentdiary.thread.SendStudentTask;

public class StudentsListActivity extends AppCompatActivity {

    private ListView studentsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);

        studentsList = (ListView) findViewById(R.id.students_list);

        studentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> list, View item, int position, long id) {
                Student student = (Student) studentsList.getItemAtPosition(position);
                Intent intentSendToStudentForm = new Intent(StudentsListActivity.this, StudentFormActivity.class);
                intentSendToStudentForm.putExtra("student", student);
                startActivity(intentSendToStudentForm);
            }
        });

        Button newStudentButton = (Button) findViewById(R.id.student_list_new_student_btn);

        newStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSendToStudentForm = new Intent(StudentsListActivity.this, StudentFormActivity.class);
                startActivity(intentSendToStudentForm);
            }
        });

        registerForContextMenu(studentsList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateStudentsList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.student_list_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_send_ratings:
                new SendStudentTask(this).execute();
                break;
            case R.id.menu_download_exams:
                Intent goToExams = new Intent(this, ExamsListActivity.class);
                startActivity(goToExams);
                break;
            case R.id.menu_show_on_maps:
                Intent goToMaps = new Intent(this, MapActivity.class);
                startActivity(goToMaps);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void populateStudentsList() {
        StudentDAO data = new StudentDAO(this);
        List<Student> studentList = data.getStudents();
        data.close();

        StudentAdapter adapter = new StudentAdapter(this, studentList);
        studentsList.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final Student student = (Student) studentsList.getItemAtPosition(info.position);

        String site = student.getWebsite();

        if (!site.startsWith("http://") && !site.startsWith("https://")) {
            site = "http://" + site;
        }

        MenuItem itemCallStudent = menu.add("Call student");
        itemCallStudent.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menu) {
                if (ActivityCompat.checkSelfPermission(StudentsListActivity.this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(StudentsListActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE}, 123);
                } else {
                    Intent intentCall = new Intent(Intent.ACTION_CALL);
                    intentCall.setData(Uri.parse("tel:" + student.getPhone()));
                    startActivity(intentCall);
                }
                return false;
            }
        });

        MenuItem itemSendSMS = menu.add("Send a SMS");
        Intent intentSMS = new Intent(Intent.ACTION_VIEW);
        intentSMS.setData(Uri.parse("sms:" + student.getPhone()));
        itemSendSMS.setIntent(intentSMS);

        MenuItem itemStudentSite = menu.add("Visit student's Website");
        Intent intentSite = new Intent(Intent.ACTION_VIEW);
        intentSite.setData(Uri.parse(site));
        itemStudentSite.setIntent(intentSite);

        MenuItem itemCheckStudentAddress = menu.add("Check Student's address on Map");
        Intent intentMap = new Intent(Intent.ACTION_VIEW);
        intentMap.setData(Uri.parse("geo:0,0?q=" + student.getAddress()));
        itemCheckStudentAddress.setIntent(intentMap);

        MenuItem delete = menu.add("Delete");
        delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                StudentDAO dao = new StudentDAO(StudentsListActivity.this);
                dao.delete(student);
                dao.close();

                populateStudentsList();

                Toast.makeText(StudentsListActivity.this, "Student " + student.getName() + " deleted", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }
}
