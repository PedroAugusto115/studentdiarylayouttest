package pedropereira.studentdiary.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import pedropereira.studentdiary.DAO.StudentDAO;
import pedropereira.studentdiary.R;
import pedropereira.studentdiary.helper.StudentFormHelper;
import pedropereira.studentdiary.model.Student;

public class StudentFormActivity extends AppCompatActivity {

    public static final int CAMERA_CODE = 567;
    private StudentFormHelper helper;
    private String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_form);

        helper = new StudentFormHelper(this);

        Intent intent = getIntent();
        Student student = (Student) intent.getSerializableExtra("student");

        if (student != null){
            helper.fillStudentForm(student);
        }

        final Button btnTakePicture = (Button) findViewById(R.id.student_form_photo_btn);

        btnTakePicture.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoPath = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + "photo.jpg";
                File PhotoFile = new File(photoPath);
                intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(PhotoFile));
                startActivityForResult(intentCamera, CAMERA_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_CODE) {
                helper.loadImage(photoPath);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.student_form_menu, menu);



        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.student_form_menu_save:
                Student student = helper.getStudent();

                StudentDAO dao = new StudentDAO(this);
                if(student.getId() != null) {
                    dao.updateStudent(student);
                }else{
                    dao.insertStudent(student);
                }

                dao.close();

                Toast.makeText(StudentFormActivity.this, "Student " + student.getName() + " saved", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
