package pedropereira.studentdiary.thread;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.List;

import pedropereira.studentdiary.DAO.StudentDAO;
import pedropereira.studentdiary.converter.StudentConverter;
import pedropereira.studentdiary.model.Student;
import pedropereira.studentdiary.web.WebClient;

/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class SendStudentTask extends AsyncTask<Void, Void, String> {
    private Context context;
    ProgressDialog dialog;

    public SendStudentTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {

        StudentDAO dao = new StudentDAO(context);
        List<Student> students = dao.getStudents();
        dao.close();

        StudentConverter converter = new StudentConverter();
        String json = converter.convertListToJson(students);

        WebClient client = new WebClient();
        String serverAnswer = client.post(json);

        return serverAnswer;
    }

    @Override
    protected void onPreExecute() {
        dialog  = ProgressDialog.show(context, "Wait", "Sending data...", true, true);
    }

    @Override
    protected void onPostExecute(String answer) {
        dialog.dismiss();
        Toast.makeText(context, answer, Toast.LENGTH_LONG).show();
    }
}
