package pedropereira.studentdiary.adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pedropereira.studentdiary.R;
import pedropereira.studentdiary.model.Student;


/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class StudentAdapter extends BaseAdapter {


    private final List<Student> students;
    private final Context context;

    public StudentAdapter(Context context, List<Student> studentList) {
        this.context = context;
        this.students = studentList;

    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Object getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return students.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Student student = students.get(position);

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = convertView;
        if(view == null){
            view = inflater.inflate(R.layout.student_list_item, parent, false);
        }


        TextView nameField = (TextView) view.findViewById(R.id.student_list_item_name);
        nameField.setText(student.getName());

        TextView phoneField = (TextView) view.findViewById(R.id.student_list_item_phone);
        phoneField.setText(student.getPhone());

        if (context.getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            TextView addressField = (TextView) view.findViewById(R.id.student_list_item_address);
            addressField.setText(student.getAddress());

            TextView webSiteField = (TextView) view.findViewById(R.id.student_list_item_website);
            webSiteField.setText(student.getWebsite());
        }

        ImageView photoField = (ImageView) view.findViewById(R.id.student_list_item_photo);
        String photoPath = student.getPhotoPath();
        if(photoPath != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(photoPath);
            Bitmap smallerBitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
            photoField.setImageBitmap(smallerBitmap);
            photoField.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        return view;
    }
}
