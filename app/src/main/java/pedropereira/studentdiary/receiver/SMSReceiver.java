package pedropereira.studentdiary.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.telephony.SmsMessage;
import android.widget.Toast;

import pedropereira.studentdiary.DAO.StudentDAO;
import pedropereira.studentdiary.R;

/**
 * Created by Pedro Augusto on 17/08/2016.
 */
public class SMSReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        Object[] pdus = (Object[]) intent.getSerializableExtra("pdus");
        byte[] pdu = (byte[]) pdus[0];
        String format = (String) intent.getSerializableExtra("format");

        SmsMessage sms = SmsMessage.createFromPdu(pdu,format);

        String smsPhone = sms.getDisplayOriginatingAddress();

        StudentDAO dao = new StudentDAO(context);

        if(dao.isStudentByPhone(smsPhone)) {
            Toast.makeText(context, "A student's SMS has arrived!", Toast.LENGTH_LONG).show();
            MediaPlayer mp = MediaPlayer.create(context, R.raw.msg);
            mp.start();
        }

        dao.close();
    }
}
